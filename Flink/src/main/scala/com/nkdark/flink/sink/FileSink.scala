package com.nkdark.flink.sink

import com.nkdark.flink.api.SensorReading
import org.apache.flink.api.common.serialization.SimpleStringEncoder
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.scala._

/**
 *
 * @author NKDark
 * Date: 2021/5/7
 * Desc:
 *
 */

object FileSink extends App {
  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
  env.setParallelism(1)

  // 0. 读取数据
  val inputPath: String = "src/main/resources/sensor.txt"
  val inputStream: DataStream[String] = env.readTextFile(inputPath)

  // 1. 先转换成样例类
  val dataStream: DataStream[SensorReading] = inputStream
    .map(data => {
      val arr: Array[String] = data.split(",")
      SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
    })

  dataStream.print()
//  dataStream.writeAsCsv("src/main/resources/out.txt")
  dataStream.addSink(
    StreamingFileSink.forRowFormat(
      new Path("src/main/resources/out"),
      new SimpleStringEncoder[SensorReading]()
    ).build()
  )

  env.execute("file sink test")
}
