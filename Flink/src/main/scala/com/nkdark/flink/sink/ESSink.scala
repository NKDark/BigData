package com.nkdark.flink.sink

import com.nkdark.flink.api.SensorReading
import com.nkdark.flink.sink.FileSink.env
import org.apache.flink.api.common.functions.RuntimeContext
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.elasticsearch.{ElasticsearchSinkFunction, RequestIndexer}
import org.apache.flink.streaming.connectors.elasticsearch7.ElasticsearchSink
import org.apache.http.HttpHost
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.Requests

import java.util

/**
 * Created by IntelliJ IDEA
 * Date: 2021/5/8
 * Desc: 
 *
 * @author NKDark
 */

object ESSink {
  def main(args: Array[String]): Unit = {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // 0. 读取数据
    val inputPath: String = "src/main/resources/sensor.txt"
    val inputStream: DataStream[String] = env.readTextFile(inputPath)

    // 1. 先转换成样例类
    val dataStream: DataStream[SensorReading] = inputStream
      .map(data => {
        val arr: Array[String] = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      })

    // 定义 HttpHosts
    val httpHosts = new util.ArrayList[HttpHost]()
    httpHosts.add(new HttpHost("cent", 9200))

    // 自定义写入 es 的 EsSinkFunction
    val myEsSinkFunction: ElasticsearchSinkFunction[SensorReading] = new ElasticsearchSinkFunction[SensorReading] {
      override def process(t: SensorReading, runtimeContext: RuntimeContext, requestIndexer: RequestIndexer): Unit = {
        // 包装一个 Map 作为 data source
        val dataSource = new util.HashMap[String, String]()
        dataSource.put("id", t.id)
        dataSource.put("temperature", t.temperature.toString)
        dataSource.put("ts", t.timestamp.toString)

        // 创建 index request，用于发送 http 请求
        val indexRequest: IndexRequest = Requests.indexRequest()
          .index("sensor")
          .source(dataSource)

        // 用 indexer 发送请求
        requestIndexer.add(indexRequest)
      }
    }

    dataStream.addSink(
      new ElasticsearchSink.Builder[SensorReading](
        httpHosts,
        myEsSinkFunction
      ).build()
    )

    env.execute("es sink test")
  }
}
