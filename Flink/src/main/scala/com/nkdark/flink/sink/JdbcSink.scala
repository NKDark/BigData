package com.nkdark.flink.sink

import com.nkdark.flink.api.SensorReading
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
import org.apache.flink.streaming.api.scala._

import java.io.FileReader
import java.sql.{Connection, DriverManager, PreparedStatement}
import java.util.Properties

/**
 * Created by IntelliJ IDEA
 * Date: 2021/5/8
 * Desc: 
 *
 * @author NKDark
 */

object JdbcSink {
  def main(args: Array[String]): Unit = {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // 0. 读取数据
    val inputPath: String = "src/main/resources/sensor.txt"
    val inputStream: DataStream[String] = env.readTextFile(inputPath)

    // 1. 先转换成样例类
    val dataStream: DataStream[SensorReading] = inputStream
      .map(data => {
        val arr: Array[String] = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      })

    dataStream.addSink(
      new MyJdbcFunction()
    )


    env.execute("es sink test")
  }
}

class MyJdbcFunction() extends RichSinkFunction[SensorReading] {
  // 定义连接、预编译语句
  var conn: Connection = _
  var insertStatement: PreparedStatement = _
  var updateStatement: PreparedStatement = _
  override def open(parameters: Configuration): Unit = {
    val prop = new Properties()
    prop.load(new FileReader("src/main/resources/jdbc.properties"))
    conn = DriverManager.getConnection("jdbc:mysql://cent/bd", prop.getProperty("username"), prop.getProperty("password"))
    insertStatement = conn.prepareStatement("INSERT INTO SENSOR_TEMP(id, temp) values (?, ?)")
    updateStatement = conn.prepareStatement("UPDATE SENSOR_TEMP SET temp = ? WHERE id = ?")
  }

  override def invoke(value: SensorReading, context: SinkFunction.Context[_]): Unit ={
    // 先执行更新操作 查到就更新
    updateStatement.setDouble(1, value.temperature)
    updateStatement.setString(2, value.id)
    updateStatement.execute()

    // 如果没有查到数据， 那么就插入
    if (0 == updateStatement.getUpdateCount) {
      insertStatement.setString(1, value.id)
      insertStatement.setDouble(2, value.temperature)
      insertStatement.execute()
    }
  }

  override def close(): Unit = {
    insertStatement.close()
    updateStatement.close()
    conn.close()
  }
}