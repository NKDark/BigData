package com.nkdark.flink.sink

import com.nkdark.flink.api.SensorReading
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011

/**
 * Created by IntelliJ IDEA
 * Date: 2021/5/8
 * Desc: 
 *
 * @author NKDark
 */

object KafkaSink extends App {
  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
  env.setParallelism(1)

  // 0. 读取数据
  val inputPath: String = "src/main/resources/sensor.txt"
  val inputStream: DataStream[String] = env.readTextFile(inputPath)

  // 1. 先转换成样例类
  val dataStream: DataStream[String] = inputStream
    .map(data => {
      val arr: Array[String] = data.split(",")
      SensorReading(arr(0), arr(1).toLong, arr(2).toDouble).toString
    })

  dataStream.addSink(
    new FlinkKafkaProducer011[String](
      "hadoop2:9092,hadoop3:9092,hadoop4:9092",
      "sink_test",
      new SimpleStringSchema()
    )
  )


  env.execute("kafka sink test")

}
