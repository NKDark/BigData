package com.nkdark.flink.api

import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011

import java.util.Properties
import scala.collection.immutable
import scala.util.Random

/**
 *
 * @author NKDark
 * Date: 2021/4/28
 * Desc:
 *
 */

// 定义样例类，温度传感器
case class SensorReading(id: String, timestamp: Long, temperature: Double)

object SourceTest {
  def main(args: Array[String]): Unit = {
    // 创建执行环境
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // 1. 从集合中读取数据
    val dataList: List[SensorReading] = List(
      SensorReading("sensor_1", 1619597323517L, 24.0),
      SensorReading("sensor_2", 1619597387517L, 15.4),
      SensorReading("sensor_3", 1619597392517L, 6.7),
      SensorReading("sensor_4", 1619597396516L, 38.3)
    )

    val stream1: DataStream[SensorReading] = env.fromCollection(dataList)

    //    stream1.print()

    // 2. 从文件中读取数据
    val inputPath: String = "src/main/resources/sensor.txt"

    val stream2: DataStream[String] = env.readTextFile(inputPath)

    //    stream2.print()

    // 3. 从 kafka 中读取数据
    val prop = new Properties()
    prop.setProperty("bootstrap.servers", "hadoop2:9092,hadoop3:9092,hadoop4:9092")
    val stream3: DataStream[String] = env.addSource(new FlinkKafkaConsumer011[String](
      "sensor",
      new SimpleStringSchema(),
      prop
    ))

    //    stream3.print()

    // 4. 自定义 Source
    val stream4: DataStream[SensorReading] = env.addSource(new MySensorSource())
    stream4.print()
    // 执行
    env.execute("source test")
  }
}

// 自定义 SourceFunction
class MySensorSource() extends SourceFunction[SensorReading] {
  // 定义一个标识位 flag ，用来表示数据源是否正常发出数据
  var running: Boolean = true

  override def run(ctx: SourceFunction.SourceContext[SensorReading]): Unit = {
    // 定义一个随机数发生器
    val random = new Random()

    // 随机生成一组(10 个) 传感器的初始温度: (id, temp)
    var curTemp: immutable.IndexedSeq[(String, Double)] = (1 to 10).map(
      i => (s"sensor_$i", random.nextDouble() * 100)
    )
    while (running) {
      // 在上次数据基础上微调，更新温度值
      curTemp = curTemp.map(
        data => {
          (data._1, data._2 + random.nextGaussian())
        }
      )
      // 获取当前时间戳，加入到数据中
      val curTime: Long = System.currentTimeMillis()
      curTemp.foreach(
        data => ctx.collect(SensorReading(data._1, curTime, data._2))
      )
      // 间隔
      Thread.sleep(1000)
    }
  }

  override def cancel(): Unit = running = false
}
