package com.nkdark.flink.api

import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time

/**
 * Created by IntelliJ IDEA
 * Date: 2021/5/10
 * Desc: 
 *
 * @author NKDark
 */

object WindowTest extends App {
  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
  env.setParallelism(1)

  // 0. 读取数据
//  val inputPath: String = "src/main/resources/sensor.txt"
//  val inputStream: DataStream[String] = env.readTextFile(inputPath)
  val inputStream: DataStream[String] = env.socketTextStream("cent", 7777)

  // 1. 先转换成样例类
  val dataStream: DataStream[SensorReading] = inputStream
    .map(data => {
      val arr: Array[String] = data.split(",")
      SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
    })

  /**
   * 窗口分配器 (window assigner)
   * window() 方法接收的输入参数是一个 WindowAssigner
   * WindowAssigner负责将每条输入的数据分发到正确的 window 中
   * Flink 提供了通用的 WindowAssigner
   *  - 滚动窗口 (tumbling window)
   *  - 滑动窗口 (sliding window)
   *  - 会话窗口 (session window)
   *  - 全局窗口 (global window)
   */

  // 每 15 秒统计一次，窗口内所有温度的最小值，以及最新的时间戳
  private val resultStream: DataStream[(String, Double, Long)] = dataStream
    .map(data => {
      (data.id, data.temperature, data.timestamp)
    })
    // 按照二元组的第一个元素（id）分组
    .keyBy(_._1)
    // 滚动时间窗口
    //    .window(TumblingEventTimeWindows.of(Time.seconds(15)))
    // 滑动时间窗口
    //    .window(SlidingProcessingTimeWindows.of(Time.seconds(15), Time.seconds(3)))
    // 会话窗口(session window)
    //    .window(EventTimeSessionWindows.withGap(Time.seconds(10)))
    // 滑动时间窗口(sliding time window)
    //    .timeWindow(Time.seconds(15), Time.seconds(5))
    // 滚动时间窗口(tumbling time window)
    .timeWindow(Time.seconds(15))
    //    .minBy(1)
    .reduce(
      (curRes, newData) => {
        (curRes._1, curRes._2.min(newData._2), newData._3)
      }
    )

  resultStream.print()
  /**
   * 窗口函数(window function)
   * - window function 定义了要对窗口中手机的数据做的计算操作
   * - 可以分为两类
   *  - 增量聚合函数(incremental aggregation functions) `优先`
   *    - 每条数据到来就进行计算，保持一个简单的状态
   *    - ReduceFunction, AggregateFunction
   *  - 全窗口函数(full window functions)
   *    - 先把窗口所有数据收集起来，等到计算的时候会遍历所有数据
   *    - ProcessWindowFunction
   */

  /**
   * 其它可选 API
   * - .trigger() -- 触发器
   *  - 定义 window 什么时候关闭，触发计算并输出结果
   * - .evictor() -- 移除器
   *  - 定义一处某些数据的逻辑
   * - .allowedLateness() -- 允许处理迟到的数据
   * - .sideOutputLateData() -- 将迟到的数据放入侧输出流
   * - .getSideOutput() -- 获取侧输出流
   */

  /**
   * window API 总览
   * - Keyed Windows
   *  - stream
   *          .keyBy(...)                     <- keyed versus non-keyed windows
   *          .window(...)                    <- required: "assigner"
   *         [.trigger(...)]                  <- optional: "trigger" (else default trigger)
   *         [.evictor(...)]                  <- optional: "evictor" (else no evictor)
   *         [.allowedLateness(...)]          <- optional: "lateness" (else zero)
   *         [.sideOutputLateData(...)]       <- optional: "output tag" (else no side output for late data)
   *          .reduce/aggregate/fold/apply()  <- required: "function"
   *         [.getSideOutput()]               <- optional: "output tag"
   *
   * - Non-Keyed Windows
   *  - stream
   *          .windowAll(...)   (不推荐)       <- required: "assigner"
   *         [.trigger(...)]                  <- optional: "trigger" (else default trigger)
   *         [.evictor(...)]                  <- optional: "evictor" (else no evictor)
   *         [.allowedLateness(...)]          <- optional: "lateness" (else zero)
   *         [.sideOutputLateData(...)]       <- optional: "output tag" (else no side output for late data)
   *          .reduce/aggregate/fold/apply()  <- required: "function"
   *         [.getSideOutput()]               <- optional: "output tag"
   */


  env.execute("window test")

}
