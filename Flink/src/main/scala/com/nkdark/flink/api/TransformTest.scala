package com.nkdark.flink.api

import org.apache.flink.api.common.functions.{MapFunction, ReduceFunction, RichMapFunction}
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.scala._

/**
 *
 * @author NKDark
 *         Date: 2021/5/3
 *         Desc: 
 *
 */

object TransformTest {
  def main(args: Array[String]): Unit = {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // 0. 读取数据
    val inputPath: String = "src/main/resources/sensor.txt"
    val inputStream: DataStream[String] = env.readTextFile(inputPath)

    // 1. 先转换成样例类
    val dataStream: DataStream[SensorReading] = inputStream
      .map(data => {
        val arr: Array[String] = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      })

    // 2. 分组聚合，输出每个传感器当前最小值
    val aggStream: DataStream[SensorReading] = dataStream
      // 根据id进行分组
      .keyBy("id")
      .min("temperature")

    // 3. 需要输出当前最小的温度值，以及最近的时间戳，要用 reduce
    val resultSteam: DataStream[SensorReading] = dataStream
      // 根据id进行分组
      .keyBy("id")
      .reduce((currentState, newData) => {
        SensorReading(currentState.id,
          newData.timestamp,
          currentState.temperature.min(newData.temperature)
        )
      })

    // 4. 多流转换操作
    // 4.1 分流， 将传感器的温度数据分成低温、高温两条流
    val splitStream: SplitStream[SensorReading] = dataStream.split(data => {
      if (data.temperature > 30.0) Seq("high") else Seq("low")
    })
    val highTempStream: DataStream[SensorReading] = splitStream.select("high")
    val lowTempStream: DataStream[SensorReading] = splitStream.select("low")
    val allTempStream: DataStream[SensorReading] = splitStream.select("high", "low")

    //    highTempStream.print("high")
    //    lowTempStream.print("low")
    //    allTempStream.print("all")

    //    aggStream.print()

    // 4.2 合流，connect
    //     connect 只能连接两个流，但是数据类型不作要求
    val warningStream: DataStream[(String, Double)] = highTempStream.map(data => (data.id, data.temperature))

    val connectedSteams: ConnectedStreams[(String, Double), SensorReading] = warningStream.connect(lowTempStream)

    // 用 coMap 对数据分别进行处理
    val coMapResultStream: DataStream[Product] = connectedSteams
      .map(
        warningData => (warningData._1, warningData._2, "warning"),
        lowTempData => (lowTempData.id, "healthy")
      )

//    coMapResultStream.print("coMap")

    // 4.3 union 合流
    //     union 可以连接多个流，要求数据类型必须相同
    val unionStream: DataStream[SensorReading] = highTempStream.union(lowTempStream)
    env.execute("transform test")
  }
}

class MyReduceFunction extends ReduceFunction[SensorReading] {
  override def reduce(t: SensorReading, t1: SensorReading): SensorReading = {
    SensorReading(t.id,
      t1.timestamp,
      t.temperature.min(t1.temperature)
    )
  }
}

/*
  UDF函数类 - 更细粒度的控制流
  函数类(Function Classes)
  Flink 暴露了所有 udf 函数的接口(实现方式为接口或者抽象类)
  例如 MapFunction, FilterFunction, ProcessFunction 等
 */

// 自定义函数类
class MyMapper extends MapFunction[SensorReading, String] {
  override def map(value: SensorReading): String = value.id + " temperature"
}

// 富函数，可以获取到运行时上下文，以及一些生命周期
class MyRichMapper extends RichMapFunction[SensorReading, String] {

  override def open(parameters: Configuration): Unit = {
    // 做一些初始化操作，比如数据库的连接
    getRuntimeContext
  }

  override def map(value: SensorReading): String = value.id + " temperature"

  override def close(): Unit = {
    // 一般做收尾工作，比如关闭连接，释放资源，清空状态
  }
}