package com.nkdark.flink.wc

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.scala._

/**
 *
 * @author NKDark
 *         Date: 2021/4/18
 *         Desc: flink streaming word count
 *
 */

object WordCountStreaming {
  def main(args: Array[String]): Unit = {
    // 创建流处理执行环境
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 设置并行度
    // env.setParallelism(16)

    // 从外部命令中提取参数，作为 socket 主机名和端口号
    val parameterTool: ParameterTool = ParameterTool.fromArgs(args)
    val host: String = parameterTool.get("host")
    val port: Int = parameterTool.getInt("port")

    // 接收一个 socket 文本流
    val inputDataStream: DataStream[String] = env.socketTextStream(hostname = host, port = port)

    // 进行转化处理统计
    val resultDataStream: DataStream[(String, Int)] = inputDataStream
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_, 1))
      .keyBy(0)
      .sum(1)

    resultDataStream.print().setParallelism(1)

    // 启动任务执行
    env.execute("Stream WordCount")
  }
}
