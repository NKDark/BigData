package com.nkdark.flink.wc

import org.apache.flink.api.scala._

/**
 *
 * @author NKDark
 * Date: 2021/4/18
 * Desc: flink word count
 */

// 批处理的 word count
object WordCount {
  def main(args: Array[String]): Unit = {
    // 创建一个批处理
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    // 从文件中读取数据
    val inputPath = "src/main/resources/hello"
    val inputDataSet: DataSet[String] = env.readTextFile(inputPath)

    // 对数据进行转换处理统计，先分词，再按照word进行分组，最后进行聚合统计
    val resultDataSet: DataSet[(String, Int)] = inputDataSet
      .flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(0)
      .sum(field = 1)

    resultDataSet.print()
  }
}
