package com.nkdark.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * 客户端代码常用
 * 1、获取一个客户端对象
 * 2、执行相关的操作命令
 * 3、关闭资源
 * HDFS zookeeper
 */
public class HdfsClient {

    private FileSystem fs;

    @Before
    public void init() throws URISyntaxException, IOException, InterruptedException {

        // 连接集群 nn 地址
        URI uri = new URI("hdfs://hadoop2:8020");

        // 创建一个配置文件
        Configuration configuration = new Configuration();

        // 用户
        String user = "nkdark";

        // 1、获取到了客户端对象
        fs = FileSystem.get(uri, configuration, user);
    }

    @After
    public void close() throws IOException {
        // 3、关闭资源
        fs.close();
    }

    @Test
    public void testMkdir() throws IOException {
        // 2、创建一个文件夹
        fs.mkdirs(new Path("/xiyou/wuzhishan"));
    }

    // 上传
    /**
     * 参数优先级 低 ==> 高
     * hdfs-default.xml ==> hdfs-site.xml ==> client中的 hdfs-site.xml ==> code
     */
    @Test
    public void testPut() throws IOException {
        fs.copyFromLocalFile(false, false, new Path("D:\\sunwukong.txt"), new Path("/xiyou/wuzhishan"));
    }

    // 下载
    @Test
    public void testGet() throws IOException {
        fs.copyToLocalFile(false, new Path("/xiyou/wuzhishan/sunwukong.txt"), new Path("D:/sunwukong-download.txt"));
    }
}
