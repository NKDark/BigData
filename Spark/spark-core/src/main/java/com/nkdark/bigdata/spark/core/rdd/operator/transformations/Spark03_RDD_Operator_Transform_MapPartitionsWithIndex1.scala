package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark03_RDD_Operator_Transform_MapPartitionsWithIndex1 {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_MapPartitionsWithIndex")

    val sc = new SparkContext(sparkConf)

    // 算子 - mapPartitionsWithIndex

    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))
    // [1, 2], [3, 4]

    // 将分区编号和内容组合打印

    val mapRDD: RDD[(Int, Int)] = rdd.mapPartitionsWithIndex(
      (index, iter) => {
        iter.map(
          num => {
            (index, num)
          }
        )
      }
    )


    mapRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
