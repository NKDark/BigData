package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark12_RDD_Operator_Transform_DoubleValue_Test {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark12_RDD_Operator_Transform_DoubleValue")

    val sc = new SparkContext(sparkConf)

    // 算子 - 双 Value 类型

    // 交集、并集、差集 要求两个数据源的数据类型保持一致
    // 拉链的两个数据源的数据类型可以不一致，返回结果为两种数据类型的 Tuple

    // Can't zip RDDs with unequal numbers of partitions: List(2, 4)
    // 两个数据源要求分区数量要保持一致

    // Can only zip RDDs with same number of elements in each partition
    // 两个数据源要求分区中数据数量保持一致

    val rdd1: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4, 5, 6), 2)
    val rdd2: RDD[Int] = sc.makeRDD(List(3, 4, 5, 6), 2)

    // 拉链
    val rdd3: RDD[(Int, Int)] = rdd1.zip(rdd2)
    println(rdd3.collect().mkString(","))

    // 关闭环境
    sc.stop()
  }

}
