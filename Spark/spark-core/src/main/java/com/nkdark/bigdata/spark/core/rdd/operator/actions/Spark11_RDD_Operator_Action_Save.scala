package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark11_RDD_Operator_Action_Save {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark11_RDD_Operator_Action_Save")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - countByKey
    val rdd: RDD[(Int, String)] = sc.makeRDD(
      List((1, "a"), (1, "a"), (1, "a"),
           (2, "b"), (3, "c"), (3, "c"))
      , 2)

    // 函数签名
    // def saveAsTextFile(path: String): Unit
    // def saveAsObjectFile(path: String): Unit
    // def saveAsSequenceFile(
    //     path: String,
    //     codec: Option[Class[_ <: CompressionCodec]] = None): Unit

    // 统计每种 key 的个数
    rdd.saveAsTextFile("output1")
    rdd.saveAsObjectFile("output2")
    // saveAsSequenceFile 方法要求数据的格式必须为 K-V 类型
    rdd.saveAsSequenceFile("output3")

    // 关闭环境
    sc.stop()
  }

}
