package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

//noinspection SpellCheckingInspection
object Spark06_RDD_Operator_Transform_GroupBy1 {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_GroupBy")

    val sc = new SparkContext(sparkConf)

    // 算子 - groupBy

    val rdd: RDD[String] = sc.makeRDD(List(
      "Hello", "狗小党", "Spark", "Scala"
    ), 2)

    // 分组和分区没有必然的关系
    val groupRDD: RDD[(Char, Iterable[String])] = rdd.groupBy(_.charAt(0))

    groupRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
