package com.nkdark.bigdata.spark.core.rdd.requirement

import org.apache.spark.rdd.RDD
import org.apache.spark.util.AccumulatorV2
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/15
 * Desc:
 *
 * @author NKDark
 */

object Spark04_Req1_HotCategoryTop10Analysis_Upgrade3 {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark04_Req1_HotCategoryTop10Analysis_Upgrade3")

    val sc = new SparkContext(sparkConf)

    // Q1: 存在大量的 shuffle 操作 (reduceByKey)
    // reduceByKey 聚合算子， spark 会提供优化，缓存

    // 1. 读取原始日志数据
    val originRDD: RDD[String] = sc.textFile("data/user_visit_action.txt")

    val acc = new HotCategoryAccumulator
    sc.register(acc)

    // 2. 将数据转换结构
    //    点击的场合 : (品类ID, (1, 0, 0))
    //    下单的场合 : (品类ID, (0, 1, 0))
    //    支付的场合 : (品类ID, (0, 0, 1))
    originRDD.foreach(
      action => {
        val data: Array[String] = action.split("_")
        if ("-1" != data(6)) {
          acc.add((data(6), "click"))
        } else if ("null" != data(8)) {
          val categoryId: Array[String] = data(8).split(",")
          categoryId.map(id => acc.add((id, "order")))
        } else if ("null" != data(10)) {
          val categoryId: Array[String] = data(10).split(",")
          categoryId.map(id => acc.add((id, "pay")))
        }
      }
    )

    // 3. 将相同的品类ID的数据进行分组聚合
    //    (品类ID, (点击数量, 下单数量, 支付数量))
    val accVal: mutable.Map[String, HotCategory] = acc.value
    val categories: Iterable[HotCategory] = accVal.values

    val sortedCategories: List[HotCategory] = categories.toList.sortWith(
      (left, right) => {
        if (left.clickCount > right.clickCount) true
        else if (left.clickCount == right.clickCount) {
          if (left.orderCount > right.orderCount) {
            true
          } else if (left.orderCount == right.orderCount) {
            left.clickCount > right.clickCount
          } else false
        } else false
      }
    )

    // 4. 将统计结果根据数量进行降序处理，取前十名
    sortedCategories.take(10)
      .foreach(println)

    sc.stop()
  }

  case class HotCategory(categoryId: String, var clickCount: Int, var orderCount: Int, var payCount: Int)

  /**
   * 自定义累加器
   * 1. 继承 AccumulatorV2，定义泛型
   * IN: (品类ID, 行为类型)
   * OUT: mutable.Map[String, HotCategory]
   *
   * 2. 重写方法
   */
  class HotCategoryAccumulator extends AccumulatorV2[(String, String), mutable.Map[String, HotCategory]] {

    private val hcMap: mutable.Map[String, HotCategory] = mutable.Map[String, HotCategory]()

    override def isZero: Boolean = hcMap.isEmpty

    override def copy(): AccumulatorV2[(String, String), mutable.Map[String, HotCategory]] = new HotCategoryAccumulator

    override def reset(): Unit = hcMap.clear()

    override def add(v: (String, String)): Unit = {
      val categoryId: String = v._1
      val actionType: String = v._2
      val category: HotCategory = hcMap.getOrElse(categoryId, HotCategory(categoryId, 0, 0, 0))
      if ("click" == actionType) {
        category.clickCount += 1
      } else if ("order" == actionType) {
        category.orderCount += 1
      } else if ("pay" == actionType) {
        category.payCount += 1
      }
      hcMap.update(categoryId, category)
    }

    override def merge(other: AccumulatorV2[(String, String), mutable.Map[String, HotCategory]]): Unit = {
      val map1: mutable.Map[String, HotCategory] = this.hcMap
      val map2: mutable.Map[String, HotCategory] = other.value

      map2.foreach {
        case (categoryId, hotCategory) =>
          val category: HotCategory = map1.getOrElse(categoryId, HotCategory(categoryId, 0, 0, 0))
          category.clickCount += hotCategory.clickCount
          category.orderCount += hotCategory.orderCount
          category.payCount += hotCategory.payCount
          map1.update(categoryId, hotCategory)
      }
    }

    override def value: mutable.Map[String, HotCategory] = hcMap
  }

}
