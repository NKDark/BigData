package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark04_RDD_Operator_Transform_FlatMap1 {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_FlatMap")

    val sc = new SparkContext(sparkConf)

    // 算子 - flatMap

    val rdd: RDD[String] = sc.makeRDD(List(
      "Hello World", "Hello Scala", "Hello Spark", "Hello you"
    ))
    // [1, 2], [3, 4]

    // flatMap 将处理的数据进行扁平化后在进行映射处理，所以算子也称之为扁平映射
    val flatRDD: RDD[String] = rdd.flatMap(
      s => {
        s.split(" ")
      }
    )


    flatRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
