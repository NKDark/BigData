package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import java.text.SimpleDateFormat

object Spark07_RDD_Operator_Transform_Filter_Test {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_Filter")

    val sc = new SparkContext(sparkConf)

    // 算子 - filter
    // 将数据根据指定的规则进行筛选过滤,符合规则的数据保留,不符合规则的数据全部丢弃
    // 当数据进行筛选过滤后,分区不变,但是分区内的数据可能不均衡,生产环境下可能会出现
    // 数据倾斜

    val rdd: RDD[String] = sc.textFile("data/apache.log")

    val sdf = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss")

    val filtRDD: RDD[String] = rdd.filter(
      _.split(" ")(3).split(":")(0) == "17/05/2015"
    )

    filtRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
