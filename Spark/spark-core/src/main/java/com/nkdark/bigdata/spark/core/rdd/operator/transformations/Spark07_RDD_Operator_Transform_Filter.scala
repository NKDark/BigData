package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark07_RDD_Operator_Transform_Filter {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_Filter")

    val sc = new SparkContext(sparkConf)

    // 算子 - filter
    // 将数据根据指定的规则进行筛选过滤,符合规则的数据保留,不符合规则的数据全部丢弃
    // 当数据进行筛选过滤后,分区不变,但是分区内的数据可能不均衡,生产环境下可能会出现
    // 数据倾斜

    val rdd: RDD[Int] = sc.makeRDD(List(
      1, 2, 3, 4, 5, 6, 7, 8, 9
    ), 2)

    val filterRDD: RDD[Int] = rdd.filter(num => num % 2 != 1)

    filterRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
