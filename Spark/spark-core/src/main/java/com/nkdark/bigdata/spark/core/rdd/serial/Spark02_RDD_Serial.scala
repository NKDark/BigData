package com.nkdark.bigdata.spark.core.rdd.serial

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/10
 * Desc: 
 *
 * @author NKDark
 */

object Spark02_RDD_Serial {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark02_RDD_Serial")
      .set("spark.serializer", "org.apache.spark.serialization.KyroSerializer")
      .registerKryoClasses(Array(classOf[Searcher]))

    val sc = new SparkContext(sparkConf)

    val rdd: RDD[String] = sc.makeRDD(Array("hello world", "hello spark", "hive", "nkdark"), 2)

    val searcher: Searcher = Searcher("hello")

    val result: RDD[String] = searcher.getMatch1(rdd)

    result.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

  // 查询对象
  case class Searcher(query: String) {

    def isMatch(s: String): Boolean = {
      s.contains(query)
    }

    // 函数序列化案例
    def getMatch1 (rdd: RDD[String]): RDD[String] = {
      rdd.filter(isMatch)
    }

    // 属性序列化案例
    def getMatch2(rdd: RDD[String]): RDD[String] = {
      val q: String = query
      rdd.filter(_.contains(q))
    }

  }

}
