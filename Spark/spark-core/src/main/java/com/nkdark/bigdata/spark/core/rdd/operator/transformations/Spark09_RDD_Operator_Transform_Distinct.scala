package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark09_RDD_Operator_Transform_Distinct {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_Distinct")

    val sc = new SparkContext(sparkConf)

    // 算子 - distinct
    // 根据指定得规则从数据集中去重

    val rdd: RDD[Int] = sc.makeRDD(List(
      1, 2, 3, 4, 5, 1, 2, 3, 4, 5
    ))

    // map(x => (x, null)).reduceByKey((x, _) => x, numPartitions).map(_._1)

    // (1, null), (2, null), (3, null), (4, null), (1, null), (2, null), (3, null), (4, null)
    // (1, null), (2, null), (3, null), (4, null)
    // (null, null) => null
    // (1, null) => 1
    val distinctRDD: RDD[Int] = rdd.distinct()

    distinctRDD.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

}
