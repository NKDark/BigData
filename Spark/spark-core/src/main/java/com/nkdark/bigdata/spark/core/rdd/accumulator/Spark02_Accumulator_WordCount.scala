package com.nkdark.bigdata.spark.core.rdd.accumulator

import org.apache.spark.rdd.RDD
import org.apache.spark.util.AccumulatorV2
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/10
 * Desc: 
 *
 * @author NKDark
 */

object Spark02_Accumulator_WordCount {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark02_Accumulator_WordCount")

    val sc = new SparkContext(sparkConf)
    val rdd: RDD[String] = sc.makeRDD(List(
        "Hello World",
        "Hello Spark",
        "Hello Scala",
    ))

    val flatRDD: RDD[String] = rdd.flatMap(_.split(" "))

    // 累加器：word count
    // 创建累加器对象
    val wcAcc = new MyAccumulator
    // 向 Spark 进行注册
    sc.register(wcAcc, "wordCountAcc")

    flatRDD.foreach(
      word => {
        wcAcc.add(word)
      }
    )

    println(wcAcc.value)

    // 获取累加器累加的结果

    sc.stop()
  }

  /**
   * 自定义数据累加器：WordCount
   * 1. 继承 AccumulatorV2，定义泛型
   *  IN： 累加器输入的数据类型
   *  OUT：累加器返回的数据类型
   *
   * 2. 重写方法
   */
  class MyAccumulator extends AccumulatorV2[String, mutable.Map[String, Long]] {

    private val wcMap = mutable.Map[String, Long]()

    override def isZero: Boolean = wcMap.isEmpty


    override def copy(): AccumulatorV2[String, mutable.Map[String, Long]] = new MyAccumulator

    override def reset(): Unit = wcMap.clear()

    // 获取累加器需要计算的值
    override def add(word: String): Unit = {
      val count: Long = wcMap.getOrElse(word, 0L) + 1
      wcMap.update(word, count)
    }

    // 合并多个累加器
    override def merge(other: AccumulatorV2[String, mutable.Map[String, Long]]): Unit = {
      val map1: mutable.Map[String, Long] = this.wcMap
      val map2: mutable.Map[String, Long] = other.value

      map2.foreach{
        case (word, count) =>
          val newCount: Long = map1.getOrElse(word, 0L) + count
          map1.update(word, newCount)
      }
    }

    // 累加器结果
    override def value: mutable.Map[String, Long] = wcMap
  }

}
