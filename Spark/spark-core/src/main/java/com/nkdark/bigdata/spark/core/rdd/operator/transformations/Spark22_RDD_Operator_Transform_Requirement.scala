package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark22_RDD_Operator_Transform_Requirement {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark21_RDD_Operator_Transform_KeyValue_Cogroup")

    val sc = new SparkContext(sparkConf)

    // 案例实操

    // 1. 获取原始数据：时间戳、省份、城市、用户、广告
    val odsRDD: RDD[String] = sc.textFile("data/agent.log")

    // 2. 将原始数据进行结构的转换。方便统计
    //    时间戳、省份、城市、用户、广告
    //    =>
    //    ( ( 省份, 广告 ), 1 )
    val mapRDD: RDD[((String, String), Int)] = odsRDD.map(
      line => {
        val data: Array[String] = line.split(" ")
        ((data(1), data(4)), 1)
      }
    )

    // 3. 将转换结构后的数据，进行分组聚合
    //    ( ( 省份, 广告 ), 1 ) => ( ( 省份, 广告 ), 总数 )
    val reduceRDD: RDD[((String, String), Int)] = mapRDD.reduceByKey(_ + _)

    // 4. 将聚合的结果进行结构的转换
    //    ( ( 省份, 广告 ), 总数 ) => ( 省份, ( 广告, 总数 ) )
    val newMapRDD: RDD[(String, (String, Int))] = reduceRDD.map {
      case ((province, ad), sum) => (province, (ad, sum))
    }

    // 5. 将转换结构后的数据根据省份进行分组
    //    ( 省份, [ ( 广告A, 总数A ), ( 广告B, 总数B ) ] )
    val groupRDD: RDD[(String, Iterable[(String, Int)])] = newMapRDD.groupByKey()

    // 6. 将分组后的数据组内排序(降序)，取前三名
    val resultRDD: RDD[(String, List[(String, Int)])] = groupRDD.mapValues(
      iter => {
        iter.toList.sortBy(_._2)(Ordering.Int.reverse).take(3)
      }
    )

    resultRDD.collect().foreach(println)



    // 关闭环境
    sc.stop()
  }

}
