package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

//noinspection SpellCheckingInspection
object Spark05_RDD_Operator_Transform_Glom {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_Glom")

    val sc = new SparkContext(sparkConf)

    // 算子 - glom

    val rdd: RDD[Int] = sc.makeRDD(List(
      1, 2, 3, 4, 5, 6, 7, 8, 9
    ), 2)
    // [1, 2], [3, 4]

    // glom 将同一个分区的数据直接转换为相同类型的内存数组进行处理，分区不变

    val glomRDD: RDD[Array[Int]] = rdd.glom()


    glomRDD.collect().foreach(data => println(data.mkString(",")))


    // 关闭环境
    sc.stop()
  }

}
