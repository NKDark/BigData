package com.nkdark.bigdata.spark.core.rdd.accumulator

import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/10
 * Desc: 
 *
 * @author NKDark
 */

object Spark01_Accumulator {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark01_Accumulator")

    val sc = new SparkContext(sparkConf)
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // reduce: 分区内计算， 分区间计算
//    val i: Int = rdd.reduce(_ + _)
//    println(i)

//    var sum = 0
//    rdd.foreach(sum + _)
//    println(s"sum = $sum")

    // 获取系统累加器
    // Spark 默认就提供了简单数据聚合的累加器
    val sumAccumulator: LongAccumulator = sc.longAccumulator("sum")

    rdd.foreach(sumAccumulator.add(_))
    // 获取累加器的值
    // 少加：转换算子中调用累加器，如果没有行动算子的话，就不会执行
    // 多加：由于全局共享累加器，多次调用行动算子的话，就会执行多次
    // 一般情况下，累加器会防止在行动算子进行操作

    println(sumAccumulator.value)

    sc.stop()
  }

}
