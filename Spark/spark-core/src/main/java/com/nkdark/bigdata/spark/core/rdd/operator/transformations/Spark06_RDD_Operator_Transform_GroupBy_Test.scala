package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import java.text.SimpleDateFormat
import java.util.Date

//noinspection SpellCheckingInspection
object Spark06_RDD_Operator_Transform_GroupBy_Test {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_GroupBy")

    val sc = new SparkContext(sparkConf)

    // 算子 - groupBy

    // 练习1：将 List("Hello", "hive", "hbase", "Hadoop") 根据单词首字母进行分组
    val rdd1: RDD[String] = sc.makeRDD(List(
      "Hello", "hive", "hbase", "Hadoop"
    ), 2)

    val groupRDD1: RDD[(Char, Iterable[String])] = rdd1.groupBy(_.charAt(0))

    groupRDD1.collect().foreach(println)

    // 练习2：从服务器日志数据 apache.log 中获取每个时间段访问量
    val sdf = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss")
    val df = new SimpleDateFormat("yyyy年MM月dd日 HH时")
    val rdd2: RDD[String] = sc.textFile("data/apache.log")
    val timeRDD2: RDD[(String, Iterable[(String, Int)])] = rdd2.map(
      data => {
        val line: Array[String] = data.split(" ")
        val timeStr: String = line(3)
        val date: Date = sdf.parse(timeStr)
        val time: String = df.format(date)
        (time, 1)
      }
    ).groupBy(_._1)
    val groupRDD2: RDD[(String, String)] = timeRDD2.map {
      case (time, iter) => (time, iter.size + "次")
    }

    groupRDD2.collect().foreach(println)

    // 练习3：WordCount
    val rdd3: RDD[String] = sc.makeRDD(List(
      "Hello", "hive", "hbase", "Hadoop", "Hello World", "狗日的小党"
    ), 2)

    val mapRDD3: RDD[(String, Int)] = rdd3.flatMap(
      _.split(" ")
    ).map(
      (_, 1)
    )

    val groupRDD3: RDD[(String, Int)] = mapRDD3.groupBy(_._1).map {
      case (word, iter) => (word, iter.size)
    }

    groupRDD3.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

}
