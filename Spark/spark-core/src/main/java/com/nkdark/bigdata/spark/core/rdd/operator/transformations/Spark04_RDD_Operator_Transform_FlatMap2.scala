package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark04_RDD_Operator_Transform_FlatMap2 {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_FlatMap2")

    val sc = new SparkContext(sparkConf)

    // 算子 - flatMap

    // 小功能：将List(List(1,2),3,List(4,5))进行扁平化操作

    val rdd: RDD[Any] = sc.makeRDD(List(
      List(1, 2), 3, List(4, 5)
    ))
    // [1, 2], [3, 4]

    // flatMap 将处理的数据进行扁平化后在进行映射处理，所以算子也称之为扁平映射

    //    val flatRDD: RDD[Any] = rdd.flatMap(
    //      data => {
    //        data match {
    //          case list: List[_] => list
    //          case datum: Any => List(datum)
    //        }
    //      }
    //    )

    val flatRDD: RDD[Any] = rdd.flatMap {
      case list: List[_] => list
      case datum: Any => List(datum)
    }


    flatRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
