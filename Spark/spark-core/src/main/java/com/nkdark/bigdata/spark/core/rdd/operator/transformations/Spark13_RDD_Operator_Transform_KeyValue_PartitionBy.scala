package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}

object Spark13_RDD_Operator_Transform_KeyValue_PartitionBy {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark13_RDD_Operator_Transform_KeyValue_PartitionBy")

    val sc = new SparkContext(sparkConf)

    // 算子 - Key-Value 类型

    // 算子 - partitionBy
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4, 5, 6, 7, 8))

    val mapRDD: RDD[(Int, Int)] = rdd.map((_, 1))
    // RDD => PairRDDFunctions
    // 隐式转换
    val newRDD: RDD[(Int, Int)] = mapRDD.partitionBy(new HashPartitioner(2))
    newRDD.saveAsTextFile("output")

    // 关闭环境
    sc.stop()
  }

}
