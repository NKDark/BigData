package com.nkdark.bigdata.spark.core.wordcount

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import java.util.Scanner

object Spark01_WordCount {

  def main(args: Array[String]): Unit = {

    // Application
    // Spark 框架
    // 建立和 Spark 框架的连接
    // JDBC: Connection
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("WordCount1")

    val sc = new SparkContext(sparkConf)

    // 执行业务操作

    // 1.读取文件，获取每行的数据
    val lines: RDD[String] = sc.textFile("data/1.txt")

    // 2.将每行的数据进行拆分，形成一个个的单词（分词）
    // 扁平化：将整体拆分成个体的操作
    val words: RDD[String] = lines.flatMap(_.split(" "))

    // 3.将数据根据单词进行分组，便于统计
    val wordGroup: RDD[(String, Iterable[String])] = words.groupBy(word => word)

    // 4.对分组后的数据进行转换
    val wordToCount: RDD[(String, Int)] = wordGroup.map {
      case (word, list) => (word, list.size)
    }

    // 5.将转换后的结果采集到控制台打印出来
    val array: Array[(String, Int)] = wordToCount.collect()
    array.foreach(println)

    // 关闭连接
    sc.stop()
  }

}
