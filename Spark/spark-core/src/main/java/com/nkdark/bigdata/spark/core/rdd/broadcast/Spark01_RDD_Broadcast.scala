package com.nkdark.bigdata.spark.core.rdd.broadcast

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

import scala.collection.mutable

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/11
 * Desc: 
 *
 * @author NKDark
 */

object Spark01_RDD_Broadcast {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark01_RDD_Broadcast")

    val sc = new SparkContext(sparkConf)

    val rdd1: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1), ("b", 2), ("c", 3)
    ))
//    val rdd2: RDD[(String, Int)] = sc.makeRDD(List(
//      ("a", 4), ("b", 5), ("c", 6)
//    ))
    val map: mutable.Map[String, Int] = mutable.Map(("a", 4), ("b", 5), ("c", 6))

    // join 会导致数据几何增长，并且会影响 shuffle 的性能，不推荐使用
//    val joinRDD: RDD[(String, (Int, Int))] = rdd1.join(rdd2)
//    joinRDD.collect().foreach(println)

    rdd1.map {
      case (word, count) =>
        val value: Int = map.getOrElse(word, 0)
        (word, (count, value))
    }.collect().foreach(println)



    sc.stop()
  }

}
