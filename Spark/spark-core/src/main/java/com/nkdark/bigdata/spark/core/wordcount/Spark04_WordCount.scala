package com.nkdark.bigdata.spark.core.wordcount

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

object Spark04_WordCount {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("WordCount4")

    val sc = new SparkContext(sparkConf)

    val odsRDD: RDD[String] = sc.textFile("data/1.txt")

    val mapRDD: RDD[String] = odsRDD.flatMap(_.split(" "))


    def wordCount1(): Unit = {

      val groupRDD: RDD[(String, Iterable[String])] = mapRDD.groupBy(word => word)

      val countResult: RDD[(String, Int)] = groupRDD.mapValues(iter => iter.size)

      countResult.foreach(println)
    }

    // groupByKey 效率不高 因为有 shuffle
    def wordCount2(): Unit = {

      val wordRDD: RDD[(String, Int)] = mapRDD.map((_, 1))

      val groupRDD: RDD[(String, Iterable[Int])] = wordRDD.groupByKey

      val countResult: RDD[(String, Int)] = groupRDD.mapValues(iter => iter.size)

      countResult.foreach(println)
    }

    // reduceByKey
    def wordCount3(): Unit = {

      val wordRDD: RDD[(String, Int)] = mapRDD.map((_, 1))

      val countResult: RDD[(String, Int)] = wordRDD.reduceByKey(_ + _)

      countResult.foreach(println)
    }

    // aggregateByKey
    def wordCount4(): Unit = {

      val wordRDD: RDD[(String, Int)] = mapRDD.map((_, 1))

      val countResult: RDD[(String, Int)] = wordRDD.aggregateByKey(0)(_ + _, _ + _)

      countResult.foreach(println)
    }

    // foldByKey
    def wordCount5(): Unit = {

      val wordRDD: RDD[(String, Int)] = mapRDD.map((_, 1))

      val countResult: RDD[(String, Int)] = wordRDD.foldByKey(0)(_ + _)

      countResult.foreach(println)
    }

    // combineByKey
    def wordCount6(): Unit = {

      val wordRDD: RDD[(String, Int)] = mapRDD.map((_, 1))

      val countResult: RDD[(String, Int)] = wordRDD.combineByKey(
        v => v,
        (x: Int, y) => x + y,
        (x: Int, y: Int) => x + y
      )

      countResult.foreach(println)
    }

    // countByKey
    def wordCount7(): Unit = {

      val wordRDD: RDD[(String, Int)] = mapRDD.map((_, 1))

      val countResult: collection.Map[String, Long] = wordRDD.countByKey()

      countResult.foreach(println)
    }

    // countByValue
    def wordCount8(): Unit = {

      val countResult: collection.Map[String, Long] = mapRDD.countByValue()

      countResult.foreach(println)
    }

    // reduce
    def wordCount9(): Unit = {
      val mapWord: RDD[mutable.Map[String, Long]] = mapRDD.map(
        word => {
          mutable.Map[String, Long]((word, 1))
        }
      )

      val countResult: mutable.Map[String, Long] = mapWord.reduce(
        (map1, map2) => {
          map2.foreach {
            case (word, count) =>
              val value: Long = map1.getOrElse(word, 0L) + count
              map1.update(word, value)
          }
          map1
        }
      )

      countResult.foreach(println)
    }

    // aggregate
    def wordCount10(): Unit = {
      val mapWord: RDD[mutable.Map[String, Long]] = mapRDD.map(
        word => {
          mutable.Map[String, Long]((word, 1))
        }
      )

      val countResult: mutable.Map[String, Long] = mapWord.aggregate(mutable.Map[String, Long]())(
        (map1, map2) => {
          map2.foreach {
            case (word, count) =>
              val value: Long = map1.getOrElse(word, 0L) + count
              map1.update(word, value)
          }
          map1
        },
        (map1, map2) => {
          map2.foreach {
            case (word, count) =>
              val value: Long = map1.getOrElse(word, 0L) + count
              map1.update(word, value)
          }
          map1
        }
      )

      countResult.foreach(println)
    }

    // fold
    def wordCount11(): Unit = {
      val mapWord: RDD[mutable.Map[String, Long]] = mapRDD.map(
        word => {
          mutable.Map[String, Long]((word, 1))
        }
      )

      val countResult: mutable.Map[String, Long] = mapWord.fold(mutable.Map[String, Long]())(
        (map1, map2) => {
          map2.foreach {
            case (word, count) =>
              val value: Long = map1.getOrElse(word, 0L) + count
              map1.update(word, value)
          }
          map1
        }
      )

      countResult.foreach(println)
    }

    wordCount1()
    println("1=========================")
    wordCount2()
    println("2=========================")
    wordCount3()
    println("3=========================")
    wordCount4()
    println("4=========================")
    wordCount5()
    println("5=========================")
    wordCount6()
    println("6=========================")
    wordCount7()
    println("7=========================")
    wordCount8()
    println("8=========================")
    wordCount9()
    println("9=========================")
    wordCount10()
    println("10=========================")
    wordCount11()
    println("11=========================")

    // 关闭连接
    sc.stop()


  }

}
