package com.nkdark.bigdata.spark.core.rdd.requirement

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/15
 * Desc:
 *
 * @author NKDark
 */

object Spark01_Req1_HotCategoryTop10Analysis {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark01_Req1_HotCategoryTop10Analysis")

    val sc = new SparkContext(sparkConf)

    // Top10 热门品类

    // 1. 读取原始日志数据
    val originRDD: RDD[String] = sc.textFile("data/user_visit_action.txt")

    // 2. 统计品类的点击数量: (品类ID, 点击数量)
    val clickActionRDD: RDD[String] = originRDD.filter(
      action => {
        val data: Array[String] = action.split("_")
        data(6) != "-1"
      }
    )

    val clickCountRDD: RDD[(String, Int)] = clickActionRDD.map(
      action => {
        val data: Array[String] = action.split("_")
        (data(6), 1)
      }
    ).reduceByKey(_ + _)

    // 3. 统计品类的下单数量: (品类ID, 下单数量)
    val orderActionRDD: RDD[String] = originRDD.filter(
      action => {
        val data: Array[String] = action.split("_")
        data(8) != null
      }
    )

    val orderCountRDD: RDD[(String, Int)] = orderActionRDD.flatMap(
      action => {
        val data: Array[String] = action.split("_")
        val categoryId: String = data(8)
        val categoryIds: Array[String] = categoryId.split(",")
        categoryIds.map(id => (id, 1))
      }
    ).reduceByKey(_ + _)


    // 4. 统计品类的支付数量: (品类ID, 支付数量)
    val payActionRDD: RDD[String] = originRDD.filter(
      action => {
        val data: Array[String] = action.split("_")
        data(10) != null
      }
    )

    val payCountRDD: RDD[(String, Int)] = orderActionRDD.flatMap(
      action => {
        val data: Array[String] = action.split("_")
        val categoryId: String = data(10)
        val categoryIds: Array[String] = categoryId.split(",")
        categoryIds.map(id => (id, 1))
      }
    ).reduceByKey(_ + _)

    // 5. 将品类进行排序，并且取前10名
    //    点击数量排序，下单数量排序，支付数量排序
    //    元组排序: 先比较第一个，再比较第二个，再比较第三个，依此类推
    //    (品类ID, (点击数量, 下单数量, 支付数量))
    //
    //    cogroup = connect + group
    val cogroupRDD: RDD[(String, (Iterable[Int], Iterable[Int], Iterable[Int]))] = clickCountRDD.cogroup(orderCountRDD, payCountRDD)

    val analysisRDD: RDD[(String, (Int, Int, Int))] = cogroupRDD.mapValues {
      case (clickIter, orderIter, payIter) =>
        var clickCount = 0
        val iter1: Iterator[Int] = clickIter.iterator
        if (iter1.hasNext) {
          clickCount = iter1.next()
        }
        var orderCount = 0
        val iter2: Iterator[Int] = orderIter.iterator
        if (iter2.hasNext) {
          orderCount = iter2.next()
        }
        var payCount = 0
        val iter3: Iterator[Int] = payIter.iterator
        if (iter3.hasNext) {
          payCount = iter3.next()
        }
        (clickCount, orderCount, payCount)
    }

    val resultRDD: Array[(String, (Int, Int, Int))] = analysisRDD.sortBy(_._2, ascending = false).take(10)

    resultRDD.foreach(println)

    sc.stop()
  }

}
