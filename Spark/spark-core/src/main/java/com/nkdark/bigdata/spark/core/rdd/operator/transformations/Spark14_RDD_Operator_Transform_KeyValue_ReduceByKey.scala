package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}

object Spark14_RDD_Operator_Transform_KeyValue_ReduceByKey {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark14_RDD_Operator_Transform_KeyValue_ReduceByKey")

    val sc = new SparkContext(sparkConf)

    // 算子 - Key-Value 类型

    // 算子 - reduceByKey
    val rdd: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1), ("a", 2), ("a", 3), ("b", 4)
    ))

    // reduceByKey: 相同的 key 的数据进行 value 数值的局和操作
    // scala 语言中一般的聚合操作都是两两聚合，所以它的聚合也是两两聚合
    // [1, 2, 3]
    // [3, 3]
    // [6]
    // reduceByKey 中如果 key 的数据只有一个，是不会参与运算的
    val resultRDD: RDD[(String, Int)] = rdd.reduceByKey((x: Int, y: Int) => {
      x + y
    })

    resultRDD.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

}
