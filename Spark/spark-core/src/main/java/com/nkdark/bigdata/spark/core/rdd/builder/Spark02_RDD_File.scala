package com.nkdark.bigdata.spark.core.rdd.builder

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark02_RDD_File {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("RDD_File")

    val sc = new SparkContext(sparkConf)

    // 创建RDD
    // 从文件中创建RDD，将文件中的数据作为处理的数据源

    // path 路径默认以当前环境的根路径为基准。也可以写绝对路径，也可以写相对路径
    // sc.textFile("F:\\Git_Repository\\BigData\\Spark\\data\\1.txt")
    // val rdd: RDD[String] = sc.textFile("data/1.txt")

    // path 路径可以是文件的具体路径，也可以是目录名称
    // val rdd: RDD[String] = sc.textFile("data")

    // path 路径还可以使用通配符*
    // val rdd: RDD[String] = sc.textFile("data/1*.txt")

    // path 路径还可以使用URL
    val rdd: RDD[String] = sc.textFile("hdfs://hadoop2:8020/directory")

    rdd.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

}
