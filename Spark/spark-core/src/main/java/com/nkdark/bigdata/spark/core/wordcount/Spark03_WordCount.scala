package com.nkdark.bigdata.spark.core.wordcount

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark03_WordCount {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("WordCount3")

    val sc = new SparkContext(sparkConf)


    val lines: RDD[String] = sc.textFile("data/1.txt")


    val words: RDD[String] = lines.flatMap(_.split(" "))

    val wordToOne: RDD[(String, Int)] = words.map(
      word => (word, 1)
    )

    // Spark 框架提供了更多的功能，可以将分组和聚合使用一个方法实现
    // reduceByKey : 相同的 key 的数据，可以对 value 进行 reduce 聚合

    // wordToOne.reduceByKey((x, y) => { x + y })
    // wordToOne.reduceByKey((x, y) =>  x + y )
    val wordToCount: RDD[(String, Int)] = wordToOne.reduceByKey(_ + _)


    val array: Array[(String, Int)] = wordToCount.collect()
    array.foreach(println)

    // 关闭连接
    sc.stop()


  }

}
