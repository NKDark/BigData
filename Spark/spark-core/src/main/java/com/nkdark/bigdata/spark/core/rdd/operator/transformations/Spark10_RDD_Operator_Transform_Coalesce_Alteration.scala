package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark10_RDD_Operator_Transform_Coalesce_Alteration {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_Coalesce")

    val sc = new SparkContext(sparkConf)

    // 算子 - coalesce
    // 根据数据量缩减分区，用于大数据集过滤后，提高小数据集的执行效率
    // 当 spark 程序中存在过多小任务的时候，可以通过 coalesce 算子，收缩合并分区，减少分区的个数，减小任务调度成本

    val rdd: RDD[Int] = sc.makeRDD(List(
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
    ), 3)

    // coalesce 也可以扩大分区数，但是一定要进行shuffle操作 不然没有意义
    // spark 提供了一个简化操作
    // 缩减分区：coalesce 如果想要数据均衡, 可以采用 shuffle
    // 扩大分区：repartition, 底层调用 coalesce 并且启用 shuffle
    // val coalesceRDD: RDD[Int] = rdd.coalesce(numPartitions = 4, shuffle = true)
    val coalesceRDD: RDD[Int] = rdd.repartition(3)

    coalesceRDD.saveAsTextFile("output")

    // 关闭环境
    sc.stop()
  }

}
