package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark11_RDD_Operator_Transform_SortBy_Test {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_SortBy")

    val sc = new SparkContext(sparkConf)

    // 算子 - sortBy

    val rdd: RDD[(String, Int)] = sc.makeRDD(List(
      ("1", 1), ("12", 2), ("3", 3), ("狗", 4), ("08", 3), ("12", 1)
    ), 2)

    // sortBy

    val sortRDD: RDD[(String, Int)] = rdd.sortBy(tuple => tuple._1)

    // sortBy 会有 shuffle 操作
    sortRDD.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

}
