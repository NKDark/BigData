package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark04_RDD_Operator_Transform_FlatMap {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_FlatMap")

    val sc = new SparkContext(sparkConf)

    // 算子 - flatMap

    val rdd: RDD[List[Int]] = sc.makeRDD(List(
      List(1, 2, 3, 4), List(5, 6, 7, 8)
    ))
    // [1, 2], [3, 4]

    // flatMap 将处理的数据进行扁平化后在进行映射处理，所以算子也称之为扁平映射
    val flatRDD: RDD[Int] = rdd.flatMap(
      list => {
        list
      }
    )


    flatRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
