package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark12_RDD_Operator_Action_Foreach {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark12_RDD_Operator_Action_Foreach")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - countByKey
    val rdd: RDD[(Int, String)] = sc.makeRDD(
      List((1, "a"), (1, "a"), (1, "a"),
           (2, "b"), (3, "c"), (3, "c"))
      , 2)

    // 函数签名
    // def foreach(f: T => Unit): Unit = withScope {
    //     val cleanF = sc.clean(f)
    //     sc.runJob(this, (iter: Iterator[T]) => iter.foreach(cleanF))
    // }

    // 统计每种 key 的个数
    // collect 后 foreach 其实是 Driver 端内存集合的循环遍历方法
    rdd.collect().foreach(println)
    println("****************")
    // 直接 foreach 是 Executor 端内存数据打印
    rdd.foreach(println)

    // 算子： Operator (操作)
    //        RDD 的方法和 Scala 集合对象的方法不一样
    //        集合对象的方法都是同一个节点的内存中完成的。
    //        RDD 的方法可以将计算逻辑发送到 Executor 端(分布式节点)执行

    //       为了区分不同的处理效果，所以将 RDD 的方法称之为算子。RDD 的方法外部的操作都是在 Driver 端执行的，
    //        而方法内部的逻辑代码实在 Executor 端执行的

    // 关闭环境
    sc.stop()
  }

}
