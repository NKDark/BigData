package com.nkdark.bigdata.spark.core.rdd.requirement

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/15
 * Desc:
 *
 * @author NKDark
 */

object Spark02_Req1_HotCategoryTop10Analysis_Upgrade1 {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark02_Req1_HotCategoryTop10Analysis_Upgrade1")

    val sc = new SparkContext(sparkConf)

    // Top10 热门品类

    // Q1: actionRDD 重复使用
    // A1: cache()

    // Q2: cogroup 性能可能较低

    // 1. 读取原始日志数据
    val originRDD: RDD[String] = sc.textFile("data/user_visit_action.txt")
    originRDD.cache()

    // 2. 统计品类的点击数量: (品类ID, 点击数量)
    val clickActionRDD: RDD[String] = originRDD.filter(
      action => {
        val data: Array[String] = action.split("_")
        data(6) != "-1"
      }
    )

    val clickCountRDD: RDD[(String, Int)] = clickActionRDD.map(
      action => {
        val data: Array[String] = action.split("_")
        (data(6), 1)
      }
    ).reduceByKey(_ + _)

    // 3. 统计品类的下单数量: (品类ID, 下单数量)
    val orderActionRDD: RDD[String] = originRDD.filter(
      action => {
        val data: Array[String] = action.split("_")
        data(8) != null
      }
    )

    val orderCountRDD: RDD[(String, Int)] = orderActionRDD.flatMap(
      action => {
        val data: Array[String] = action.split("_")
        val categoryId: String = data(8)
        val categoryIds: Array[String] = categoryId.split(",")
        categoryIds.map(id => (id, 1))
      }
    ).reduceByKey(_ + _)


    // 4. 统计品类的支付数量: (品类ID, 支付数量)
    val payActionRDD: RDD[String] = originRDD.filter(
      action => {
        val data: Array[String] = action.split("_")
        data(10) != null
      }
    )

    val payCountRDD: RDD[(String, Int)] = orderActionRDD.flatMap(
      action => {
        val data: Array[String] = action.split("_")
        val categoryId: String = data(10)
        val categoryIds: Array[String] = categoryId.split(",")
        categoryIds.map(id => (id, 1))
      }
    ).reduceByKey(_ + _)

    // (品类ID, 点击数量) => (品类ID, (点击数量, 0, 0))
    // (品类ID, 下单数量) => (品类ID, (0, 下单数量, 0))
    //                  => (品类ID, (点击数量, 下单数量, 0))
    // (品类ID, 支付数量) => (品类ID, (0, 0, 支付数量))
    //                  => (品类ID, (点击数量, 下单数量, 支付数量))
    // (品类ID, (点击数量, 下单数量, 支付数量)

    // 5. 将品类进行排序，并且取前10名
    //    点击数量排序，下单数量排序，支付数量排序
    //    元组排序: 先比较第一个，再比较第二个，再比较第三个，依此类推
    //    (品类ID, (点击数量, 下单数量, 支付数量))
    //
    //    cogroup = connect + group
    //
    //    cogroup 有可能存在 shuffle

    val clickCountRDD1: RDD[(String, (Int, Int, Int))] = clickCountRDD.map {
      case (categoryId, count) => (categoryId, (count, 0, 0))
    }

    val orderCountRDD1: RDD[(String, (Int, Int, Int))] = orderCountRDD.map {
      case (categoryId, count) => (categoryId, (0, count, 0))
    }

    val payCountRDD1: RDD[(String, (Int, Int, Int))] = payCountRDD.map {
      case (categoryId, count) => (categoryId, (0, 0, count))
    }

    // 将三个数据源合并在一起，统一进行聚合计算
    val sourceRDD: RDD[(String, (Int, Int, Int))] = clickCountRDD1.union(orderCountRDD1).union(payCountRDD1)

    val analysisRDD: RDD[(String, (Int, Int, Int))] = sourceRDD.reduceByKey(
      (t1, t2) => {
        (t1._1 + t2._1, t1._2 + t2._2, t1._3 + t2._3)
      }
    )

    val resultRDD: Array[(String, (Int, Int, Int))] = analysisRDD.sortBy(_._2, ascending = false).take(10)

    resultRDD.foreach(println)

    sc.stop()
  }
}
