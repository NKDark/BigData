package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark07_RDD_Operator_Action_TakeOrdered {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark07_RDD_Operator_Action_TakeOrdered")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - takeOrdered
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 函数签名
    // def takeOrdered(num: Int)(implicit ord: Ordering[T]): Array[T]

    // 返回该 RDD 排序后的前 n 个元素组成的数组
    val takeOrderedResult: Array[Int] = rdd.takeOrdered(2)(Ordering.Int.reverse)

    println(takeOrderedResult.mkString(","))

    // 关闭环境
    sc.stop()
  }

}
