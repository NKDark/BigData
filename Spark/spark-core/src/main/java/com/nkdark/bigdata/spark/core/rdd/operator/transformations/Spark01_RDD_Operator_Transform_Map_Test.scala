package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark01_RDD_Operator_Transform_Map_Test {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform")

    val sc = new SparkContext(sparkConf)

    // 算子 - map
    val rdd: RDD[String] = sc.textFile("data/apache.log")

    // 长的字符串 => 短的字符串
    val mapRDD: RDD[String] = rdd.map(
      line => {
        val data: Array[String] = line.split(" ")
        data(6)
      }
    )


    mapRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
