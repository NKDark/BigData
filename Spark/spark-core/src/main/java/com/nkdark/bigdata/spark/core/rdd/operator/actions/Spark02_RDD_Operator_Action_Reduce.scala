package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/8
 * Desc: 
 *
 * @author NKDark
 */

object Spark02_RDD_Operator_Action_Reduce {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark02_RDD_Operator_Action_Reduce")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - reduce
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 函数签名
    // def reduce(f: (T, T) => T): T

    // 聚集 RDD 中的所有元素，先聚合分区内数据，再聚合分区间数据
    val i: Int = rdd.reduce(_ + _)

    println(i)

    // 关闭环境
    sc.stop()
  }

}
