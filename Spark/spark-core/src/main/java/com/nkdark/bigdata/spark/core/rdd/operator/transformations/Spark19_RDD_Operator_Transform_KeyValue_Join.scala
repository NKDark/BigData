package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark19_RDD_Operator_Transform_KeyValue_Join {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark19_RDD_Operator_Transform_KeyValue_Join")

    val sc = new SparkContext(sparkConf)

    // 算子 - Key-Value 类型

    // 算子 - aggregateByKey
    val rdd1: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1), ("b", 2), ("c", 3)
    ))

    val rdd2: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 4), ("b", 5), ("c", 6)
    ))

    // join: 两个不同数据源的数据，相同的 key 的 value 会连接在一起，形成元组
    //       如果两个数据源中 key 没有匹配上，那么数据不会出现在结果中
    //       如果两个数据源中 key 有多个相同的，会一次匹配，可能会出现笛卡尔乘积，数据量会几何形增长，会导致性能降低

    val joinRDD: RDD[(String, (Int, Int))] = rdd1.join(rdd2)

    joinRDD.collect().foreach(println)



    // 关闭环境
    sc.stop()
  }

}
