package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark08_RDD_Operator_Transform_Sample {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_Sample")

    val sc = new SparkContext(sparkConf)

    // 算子 - sample
    // 根据指定得规则从数据集中抽取数据
//    def sample(
//                withReplacement: Boolean, // true -> 抽取后放回， false -> 抽取后不放回
//                fraction: Double, // 分数 比率
//                seed: Long = Utils.random.nextLong // 随机数种子
    //): RDD[T] = {
//      require(fraction >= 0,
//        s"Fraction must be nonnegative, but got ${fraction}")
    // 三个参数
    // 1. 第一个参数表示是否将数据放回
    // 2. 第二个参数表示:
    //             1. 如果抽取不放回（伯努利算法）：数据源中每条数据被抽取的概率，基准值的概念
    //                    具体实现：根据种子和随机算法算出一个数和第二个参数设置的几率比较，小于第二个参数要，大于不要
    //                    抽取的几率，范围在[0, 1]，0：全不取、1：全取
    //             2. 如果抽取放回（泊松算法）    ：数据源中的每条数据被抽取的可能次数
    //                    重复数据的几率，范围大于等于0，表示每一个元素被期望抽取到的次数
    // 3. 第三个参数表示抽取数据时随机算法的种子

    val rdd: RDD[Int] = sc.makeRDD(List(
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    ))

    val sampleRDD: RDD[Int] = rdd.sample(
      withReplacement = false,
      fraction = 0.4,
      seed = 1
    )

    println(sampleRDD.collect().mkString(","))

    // 常用来解决数据倾斜


    // 关闭环境
    sc.stop()
  }

}
