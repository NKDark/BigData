package com.nkdark.bigdata.spark.core.rdd.io

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/10
 * Desc: 
 *
 * @author NKDark
 */

object Spark01_RDD_IO_Save {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark01_RDD_IO_Save")

    val sc = new SparkContext(sparkConf)

    val rdd: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1),
      ("b", 2),
      ("c", 3),
    ))

    rdd.saveAsTextFile("output1")
    rdd.saveAsObjectFile("output2")
    rdd.saveAsSequenceFile("output3")
  }

}
