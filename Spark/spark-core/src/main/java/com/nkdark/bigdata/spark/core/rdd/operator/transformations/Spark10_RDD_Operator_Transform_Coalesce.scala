package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark10_RDD_Operator_Transform_Coalesce {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_Coalesce")

    val sc = new SparkContext(sparkConf)

    // 算子 - coalesce
    // 根据数据量缩减分区，用于大数据集过滤后，提高小数据集的执行效率
    // 当 spark 程序中存在过多小任务的时候，可以通过 coalesce 算子，收缩合并分区，减少分区的个数，减小任务调度成本

    val rdd: RDD[Int] = sc.makeRDD(List(
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
    ), 6)

    // coalesce 默认不会打乱分区重新组合数据
    // 这种情况下的缩减分区可能会导致数据不均衡，出现数据倾斜
    // 如果想让数据均衡，可以进行 shuffle 处理
//    val coalesceRDD: RDD[Int] = rdd.coalesce(numPartitions = 2)
    val coalesceRDD: RDD[Int] = rdd.coalesce(numPartitions = 2, shuffle = true)

    coalesceRDD.saveAsTextFile("output")

    // 关闭环境
    sc.stop()
  }

}
