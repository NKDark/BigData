package com.nkdark.bigdata.spark.core.rdd.builder

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark01_RDD_Memory_Par {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("RDD_Memory_Par")

    val sc = new SparkContext(sparkConf)

    // 创建RDD
    // RDD 的并行度 & 分区
    // markRDD 方法可以传递第二个参数，这个参数表示分区的数量
    // 第二个参数是可选参数，如果不传参，则会使用默认值：defaultParallelism（默认并行度）=
    // scheduler.conf.getInt("spark.default.parallelism", totalCores)
    // spark 在默认情况下，从配置对象中获取配置参数：spark.default.parallelism
    // 如果获取不到，那么使用totalCores属性，这个属性取值为当前运行环境的最大可用核心数
    // conf.getInt("spark.default.parallelism", math.max(totalCoreCount.get(), 2))
    val rdd: RDD[Int] = sc.makeRDD(
      List(1, 2, 3, 4), 2
    )

    // 将处理的数据保存成分区文件
    rdd.saveAsTextFile("output")

    // 关闭环境
    sc.stop()
  }

}
