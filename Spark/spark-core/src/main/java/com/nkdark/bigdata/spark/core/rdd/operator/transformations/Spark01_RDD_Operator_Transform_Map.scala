package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark01_RDD_Operator_Transform_Map {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform")

    val sc = new SparkContext(sparkConf)

    // 算子 - map

    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 1, 2, 3, 4
    // 2, 4, 6, 8

    //    def mapFunction(num: Int): Int = {
    //      num * 2
    //    }


    //    val mapRDD: RDD[Int] = rdd.map(mapFunction)
    // val mapRDD: RDD[Int] = rdd.map((num: Int) => {num * 2})
    // val mapRDD: RDD[Int] = rdd.map((num: Int) => {num * 2})
    // val mapRDD: RDD[Int] = rdd.map((num) => num * 2)
    // val mapRDD: RDD[Int] = rdd.map(num => num * 2)
    val mapRDD: RDD[Int] = rdd.map(_ * 2)


    mapRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
