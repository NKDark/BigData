package com.nkdark.bigdata.spark.core.rdd.builder

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark01_RDD_Memory {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("RDD_Memory")

    val sc = new SparkContext(sparkConf)

    // 创建RDD
    // 从内存中创建RDD，将内存中集合的数据作为处理的数据源
    val seq: Seq[Int] = Seq[Int](1, 2, 3, 4)

    // parallelize: 并行
    // val rdd: RDD[Int] = sc.parallelize(seq)
    // makeRDD 方法在底层实现时实际上是调用了 rdd 对象的 parallelize 方法
    val rdd: RDD[Int] = sc.makeRDD(seq)

    rdd.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

}
