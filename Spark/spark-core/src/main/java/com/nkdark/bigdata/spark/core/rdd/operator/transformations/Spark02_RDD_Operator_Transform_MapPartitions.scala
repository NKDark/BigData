package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark02_RDD_Operator_Transform_MapPartitions {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_MapPartitions")

    val sc = new SparkContext(sparkConf)

    // 算子 - mapPartitions

    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4), 2)

    // mapPartitions：可以以分区为单位进行数据转换操作
    //                但是会将整个分区的数据加载到内存进行引用
    //                由于存在对于对象的引用，所以处理完的数据是不会被释放掉的
    //                在内存较小，数据量较大的场合下，容易出现内存溢出

    val mapRDD: RDD[Int] = rdd.mapPartitions(
      (data: Iterator[Int]) => {
        data.filter(_ % 2 == 0).map(_ * 3)
      }
    )


    mapRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
