package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark16_RDD_Operator_Transform_KeyValue_AggregateByKey_Test {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark16_RDD_Operator_Transform_KeyValue_AggregateByKey")

    val sc = new SparkContext(sparkConf)

    // 算子 - Key-Value 类型

    // 算子 - aggregateByKey
    val rdd: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1), ("a", 2), ("b", 3),
      ("b", 4), ("b", 5), ("a", 6)
    ), 2)

    // aggregateByKey 最终的返回数据结果应该和初始值的类型保持一致
    // val aggRDD: RDD[(String, Int)] = rdd.aggregateByKey(0)(_ + _, _ + _)
    // aggRDD.collect().foreach(println)

    // 获取相同 key 的数据的平均值 => (a, 3), (b, 4)
    val aggRDD: RDD[(String, (Int, Int))] = rdd.aggregateByKey((0, 0))(
      (t, v) => (t._1 + v, t._2 + 1),
      (t1, t2) => (t1._1 + t2._1, t1._2 + t2._2)
    )

    val resultRDD: RDD[(String, Int)] = aggRDD.mapValues {
      case (num, count) => num / count
    }

    resultRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
