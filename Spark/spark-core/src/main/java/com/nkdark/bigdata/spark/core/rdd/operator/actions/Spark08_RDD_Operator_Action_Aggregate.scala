package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark08_RDD_Operator_Action_Aggregate {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark08_RDD_Operator_Action_Aggregate")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - aggregate
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4), 2)

    // 函数签名
    // def aggregate[U: ClassTag](zeroValue: U)(seqOp: (U, T) => U, combOp: (U, U) => U): U

    // 分区的数据通过初始值和分区内的数据进行聚合，然后再和初始值进行分区间的数据聚合
    val aggregateResult: Int = rdd.aggregate(10)(_ + _, _ + _)

    println(aggregateResult)

    // 关闭环境
    sc.stop()
  }

}
