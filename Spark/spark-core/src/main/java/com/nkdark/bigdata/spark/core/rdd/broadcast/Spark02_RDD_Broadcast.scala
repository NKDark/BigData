package com.nkdark.bigdata.spark.core.rdd.broadcast

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/14
 * Desc: 
 *
 * @author NKDark
 */

object Spark02_RDD_Broadcast {

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark02_RDD_Broadcast")

    val sc = new SparkContext(sparkConf)

    val rdd1: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1), ("b", 2), ("c", 3)
    ))
    val map: mutable.Map[String, Int] = mutable.Map(("a", 4), ("b", 5), ("c", 6))

    // 封装广播变量
    val broadcast: Broadcast[mutable.Map[String, Int]] = sc.broadcast[mutable.Map[String, Int]](map)

    rdd1.map {
      case (word, count) =>
        val value: Int = broadcast.value.getOrElse(word, 0)
        (word, (count, value))
    }.collect().foreach(println)

    sc.stop()
  }

}
