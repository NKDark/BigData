package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark15_RDD_Operator_Transform_KeyValue_GroupByKey {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark15_RDD_Operator_Transform_KeyValue_GroupByKey")

    val sc = new SparkContext(sparkConf)

    // 算子 - Key-Value 类型

    // 算子 - groupByKey
    val rdd: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1), ("a", 2), ("a", 3), ("b", 4)
    ))

    // groupByKey: 将数据源中的数据，相同的 key 的数据分在一个组中，形成一个对偶元组
    //             元组中的第一个元素就是 key，
    //             元组中的第二个元素就是相同 key 的 value 的集合
    val groupRDD: RDD[(String, Iterable[Int])] = rdd.groupByKey()

    groupRDD.collect().foreach(println)

    val groupRDD1: RDD[(String, Iterable[(String, Int)])] = rdd.groupBy(_._1)

    groupRDD1.collect().foreach(println)

    // groupByKey 和 reduceByKey 的区别

    // 从 shuffle 角度：reduceByKey 和 groupByKey 都存在 shuffle 的操作，但是 reduceByKey
    // 可以在 shuffle 前对分区内相同 key 的数据进行预聚合(combine)功能，这样会减少落盘的数据量，
    // 而 groupByKey 只是进行分组，不存在数据量减少的问题，reduceByKey 性能比较高。

    // 从功能的角度：reduceByKey 其实包含分组和聚合的功能，groupByKey 只能分组，不能聚合，所以在
    // 分组聚合的场景下，推荐使用 reduceByKey，如果仅仅是分组而不需要聚合。那么还是只能使用 groupByKey

    // 关闭环境
    sc.stop()
  }

}
