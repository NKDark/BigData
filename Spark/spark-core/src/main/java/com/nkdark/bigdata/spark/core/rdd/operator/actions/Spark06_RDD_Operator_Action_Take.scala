package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark06_RDD_Operator_Action_Take {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark06_RDD_Operator_Action_Take")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - take
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 函数签名
    // def take(num: Int): Array[T]

    // 返回一个由 RDD 的前 n 个元素组成的数组
    val takeResult: Array[Int] = rdd.take(2)

    println(takeResult.mkString(","))

    // 关闭环境
    sc.stop()
  }

}
