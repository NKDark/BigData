package com.nkdark.bigdata.spark.core.rdd.requirement

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/15
 * Desc: 
 *
 * @author NKDark
 */

object Spark05_Req2_HotCategoryTop10SessionAnalysis {

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark05_Req2_HotCategoryTop10SessionAnalysis")

    val sc = new SparkContext(sparkConf)
    val originRDD: RDD[String] = sc.textFile("data/user_visit_action.txt")
    originRDD.cache()
    val top10Ids: Array[String] = top10Category(originRDD)

    // 1. 过滤原始数据, 保留点击和前10品类ID
    val filterActionRDD: RDD[String] = originRDD.filter(
      action => {
        val data: Array[String] = action.split("_")
        if ("-1" != data(6)) {
          top10Ids.contains(data(6))
        } else false
      }
    )

    // 2. 根据品类ID和session
    val reduceRDD: RDD[((String, String), Int)] = filterActionRDD.map(
      action => {
        val data: Array[String] = action.split("_")
        ((data(6), data(2)), 1)
      }
    ).reduceByKey(_ + _)

    // 3. 将统计的结果进行结构的转换
    // ((品类ID, sessionId), sum) => (品类ID, (sessionId, sum))
    val mapRDD: RDD[(String, (String, Int))] = reduceRDD.map {
      case ((categoryId, sessionId), sum) => (categoryId, (sessionId, sum))
    }

    // 4. 相同的品类进行分组
    val groupRDD: RDD[(String, Iterable[(String, Int)])] = mapRDD.groupByKey()

    // 5. 将分组后的数据进行点击量的排序，并且取前10名
    val resultRDD: RDD[(String, List[(String, Int)])] = groupRDD.mapValues(
      iter => iter.toList.sortBy(_._2)(Ordering.Int.reverse).take(10)
    )

    resultRDD.collect().foreach(println)

    sc.stop()
  }

  def top10Category(dataRDD: RDD[String]): Array[String] = {
    val flatRDD: RDD[(String, (Int, Int, Int))] = dataRDD.flatMap(
      action => {
        val data: Array[String] = action.split("_")
        if ("-1" != data(6)) {
          List((data(6), (1, 0, 0)))
        } else if ("null" != data(8)) {
          val categoryId: Array[String] = data(8).split(",")
          categoryId.map(id => (id, (0, 1, 0)))
        } else if ("null" != data(10)) {
          val categoryId: Array[String] = data(10).split(",")
          categoryId.map(id => (id, (0, 0, 1)))
        } else {
          Nil
        }
      }
    )
    val reduceRDD: RDD[(String, (Int, Int, Int))] = flatRDD.reduceByKey(
      (t1, t2) => {
        (t1._1 + t2._1, t1._2 + t2._2, t1._3 + t2._3)
      }
    )
    reduceRDD.sortBy(_._2, ascending = false).take(10).map(_._1)
  }

}
