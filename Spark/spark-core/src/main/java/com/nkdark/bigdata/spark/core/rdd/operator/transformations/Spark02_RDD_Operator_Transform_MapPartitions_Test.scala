package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark02_RDD_Operator_Transform_MapPartitions_Test {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_MapPartitions")

    val sc = new SparkContext(sparkConf)

    // 算子 - mapPartitions

    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4), 2)

    // 获取每个数据分区的最大值
    val mapRDD: RDD[Int] = rdd.mapPartitions(
      (iter: Iterator[Int]) => {
        List(iter.max).iterator
      }
    )

    mapRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
