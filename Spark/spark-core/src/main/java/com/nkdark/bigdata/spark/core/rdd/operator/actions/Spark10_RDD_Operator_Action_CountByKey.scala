package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark10_RDD_Operator_Action_CountByKey {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark10_RDD_Operator_Action_CountByKey")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - countByKey
    val rdd: RDD[(Int, String)] = sc.makeRDD(
      List((1, "a"), (1, "a"), (1, "a"),
           (2, "b"), (3, "c"), (3, "c"))
      , 2)

    // 函数签名
    // def countByKey(): Map[K, Long]

    // 统计每种 key 的个数
    val countResult: collection.Map[Int, Long] = rdd.countByKey()

    println(countResult)

    // 关闭环境
    sc.stop()
  }

}
