package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark17_RDD_Operator_Transform_KeyValue_FoldByKey {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark17_RDD_Operator_Transform_KeyValue_FoldByKey")

    val sc = new SparkContext(sparkConf)

    // 算子 - Key-Value 类型

    // 算子 - foldByKey
    val rdd: RDD[(String, Int)] = sc.makeRDD(List(
      ("a", 1), ("a", 2), ("a", 3), ("b", 4)
    ), 2)

    // aggregateByKey 存在函数柯里化，有两个参数列表
    // 第一个参数列表,需要传递一个参数，表示为初始值
    //      主要用于当碰见第一个 key 的时候，和 value 进行分区内计算

    // 第二个参数列表 需要传递两个参数
    //      第一个参数表示分区内计算规则
    //      第二个参数表示分区间计算规则
    val aggRDD: RDD[(String, Int)] = rdd.aggregateByKey(0)(_ + _, _ + _)

    aggRDD.collect().foreach(println)

    val foldRDD: RDD[(String, Int)] = rdd.foldByKey(0)(_ + _)

    foldRDD.collect().foreach(println)

    // 关闭环境
    sc.stop()
  }

}
