package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark11_RDD_Operator_Transform_SortBy {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_SortBy")

    val sc = new SparkContext(sparkConf)

    // 算子 - sortBy 将数据根据指定的规则进行排序
    // 第一个参数为排序的规则
    // 第二个参数为是否升序排序

    val rdd: RDD[Int] = sc.makeRDD(List(
      2, 1, 4, 3, 5, 12, 8, 6, 11, 7, 9, 10
    ), 2)

    // sortBy

    val sortRDD: RDD[Int] = rdd.sortBy(
      f = number => number,
      // 不填默认值为 true
      ascending = true
    )

    // sortBy 会有 shuffle 操作
    // 默认不会改变分区
    sortRDD.saveAsTextFile("output")

    // 关闭环境
    sc.stop()
  }

}
