package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark09_RDD_Operator_Action_Fold {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark09_RDD_Operator_Action_Fold")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - fold
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4), 2)

    // 函数签名
    // def fold(zeroValue: T)(op: (T, T) => T): T

    // 折叠操作，aggregate 的简化版操作
    val foldResult: Int = rdd.fold(10)(_ + _)

    println(foldResult)

    // 关闭环境
    sc.stop()
  }

}
