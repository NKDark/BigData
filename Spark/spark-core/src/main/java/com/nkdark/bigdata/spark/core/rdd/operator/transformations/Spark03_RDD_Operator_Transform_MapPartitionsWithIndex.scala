package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark03_RDD_Operator_Transform_MapPartitionsWithIndex {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_MapPartitionsWithIndex")

    val sc = new SparkContext(sparkConf)

    // 算子 - mapPartitionsWithIndex

    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4), 2)
    // [1, 2], [3, 4]

    // mapPartitionsWithIndex 将待处理的数据以分区为单位发送到计算节点进行处理，
    //                        这里的处理是指可以进行任意的处理，哪怕是过滤数据，
    //                        在处理时同时可以获取当前分区索引
    val mapRDD: RDD[Int] = rdd.mapPartitionsWithIndex(
      (index, iter) => {
        if (1 == index) {
          iter
        } else {
          Nil.iterator
        }
      }
    )


    mapRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
