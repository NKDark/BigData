package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/8
 * Desc: 
 *
 * @author NKDark
 */

object Spark04_RDD_Operator_Action_Count {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark04_RDD_Operator_Action_Count")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - count
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 函数签名
    // def count(): Long

    // 返回 RDD 中元素的个数
    val count: Long = rdd.count()

    println(count)

    // 关闭环境
    sc.stop()
  }

}
