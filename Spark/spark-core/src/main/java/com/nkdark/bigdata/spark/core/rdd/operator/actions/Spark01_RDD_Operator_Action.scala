package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/8
 * Desc: 
 *
 * @author NKDark
 */

object Spark01_RDD_Operator_Action {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark01_RDD_Operator_Action")

    val sc = new SparkContext(sparkConf)

    // 行动算子
    // 所谓的行动算子，其实就是触发作业(Job)执行的方法
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 底层代码调用的是环境对象的 runJob() 方法
    // 底层代码会创建 ActiveJob ，并提交执行。
    rdd.collect()

    // 关闭环境
    sc.stop()
  }

}
