package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/9
 * Desc: 
 *
 * @author NKDark
 */

object Spark05_RDD_Operator_Action_First {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark05_RDD_Operator_Action_First")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - first
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 函数签名
    // def first(): T

    // 返回 RDD 中的第一个元素
    val firstResult: Int = rdd.first()

    println(firstResult)

    // 关闭环境
    sc.stop()
  }

}
