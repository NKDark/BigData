package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark12_RDD_Operator_Transform_DoubleValue {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark12_RDD_Operator_Transform_DoubleValue")

    val sc = new SparkContext(sparkConf)

    // 算子 - 双 Value 类型

    // 交集、并集、差集 要求两个数据源的数据类型保持一致
    // 拉链的两个数据源的数据类型可以不一致，返回结果为两种数据类型的 Tuple

    val rdd1: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))
    val rdd2: RDD[Int] = sc.makeRDD(List(3, 4, 5, 6))

    // 交集 (3, 4)
    val rdd3: RDD[Int] = rdd1.intersection(rdd2)
    println(rdd3.collect().mkString(","))

    // 并集 (1, 2, 3, 4, 3, 4, 5, 6)
    val rdd4: RDD[Int] = rdd1.union(rdd2)
    println(rdd4.collect().mkString(","))

    // 差集 (1, 2)
    val rdd5: RDD[Int] = rdd1.subtract(rdd2)
    println(rdd5.collect().mkString(","))

    // 拉链 ((1, 3), (2, 4), (3, 5), (4, 6))
    val rdd6: RDD[(Int, Int)] = rdd1.zip(rdd2)
    println(rdd6.collect().mkString(","))

    // 关闭环境
    sc.stop()
  }

}
