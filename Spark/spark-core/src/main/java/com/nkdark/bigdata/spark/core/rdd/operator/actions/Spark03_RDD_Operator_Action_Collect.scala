package com.nkdark.bigdata.spark.core.rdd.operator.actions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by IntelliJ IDEA
 * Date: 2021/9/8
 * Desc: 
 *
 * @author NKDark
 */

object Spark03_RDD_Operator_Action_Collect {

  def main(args: Array[String]): Unit = {
    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Spark03_RDD_Operator_Action_Collect")

    val sc = new SparkContext(sparkConf)

    // 行动算子 - collect
    val rdd: RDD[Int] = sc.makeRDD(List(1, 2, 3, 4))

    // 函数签名
    // def collect(): Array[T]

    // 在驱动程序中，以数组 Array 的形式返回数据集的所有元素
    val ints: Array[Int] = rdd.collect()

    println(ints.mkString(", "))

    // 关闭环境
    sc.stop()
  }

}
