package com.nkdark.bigdata.spark.core.rdd.operator.transformations

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

//noinspection SpellCheckingInspection
object Spark06_RDD_Operator_Transform_GroupBy {

  def main(args: Array[String]): Unit = {

    // 准备环境
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Operator_Transform_GroupBy")

    val sc = new SparkContext(sparkConf)

    // 算子 - groupBy

    val rdd: RDD[Int] = sc.makeRDD(List(
      1, 2, 3, 4, 5, 6, 7, 8, 9
    ), 2)
    // [1, 2], [3, 4]

    // groupBy 将数据根据指定的规则进行分组，分区默认不变，但是数据会被打乱重新组合，
    //         这个操作被称为shuffle。极限情况下，数据可能被分在同一个分区中

    // groupBy会将数据源中的每一个数据进行分组判断，根据返回的分组key进行分组
    // 相同的 key 值的数据会放置在一个组中
    def groupFun(num: Int): Int = {
      num % 2
    }

    val groupRDD: RDD[(Int, Iterable[Int])] = rdd.groupBy(groupFun)

    groupRDD.collect().foreach(println)


    // 关闭环境
    sc.stop()
  }

}
